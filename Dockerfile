FROM debian:9

RUN DEBIAN_FRONTEND=noninteractive apt-get update \
 && apt-get install -y \
      binutils-arm-linux-gnueabi \
      build-essential \
      gcc-arm-linux-gnueabi \
      libc-dev-armel-cross \
      make \
      mingw-w64 \
 && rm -rf /var/lib/apt/* /var/cache/apt/*

